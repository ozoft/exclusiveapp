package comexclusive.ozofttechnologies.exclusiveapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {

    private TextView mUserName, mUserId;
    private Button mSignOut, mAboutButton;

    private FirebaseAuth mAuth;
    private DatabaseReference UsersRef, mCurrentUserlog;

    private String mCurrentUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() == null) {
            sendUserToLoginActivity();
        } else {
            mCurrentUserId = mAuth.getCurrentUser().getUid();
            UsersRef = FirebaseDatabase.getInstance().getReference().child("Users");
            mCurrentUserlog = FirebaseDatabase.getInstance().getReference().child("Users").child(mCurrentUserId);

            //mSignOut = (Button) findViewById(R.id.sign_out);
            mUserName = (TextView) findViewById(R.id.person_name);
            mUserId = (TextView) findViewById(R.id.user_number);
            mAboutButton = (Button) findViewById(R.id.about_button);

            mCurrentUserlog.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        String userNumber = dataSnapshot.child("memberNumber").getValue().toString();
                        String userName = dataSnapshot.child("userName").getValue().toString();

                        mUserName.setText(userName);
                        mUserId.setText(
                                getApplication().getApplicationContext().getResources().getString(R.string.MemberNumber)
                                        + " " + userNumber);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            mAboutButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    sendUserToAboutActivity();
                }
            });

            // This code is temporary
            /*mSignOut.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mAuth.signOut();
                    sendUserToLoginActivity();
                }
            });*/
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser mCurrentUser = mAuth.getCurrentUser();

        if (mCurrentUser == null) {
            sendUserToLoginActivity();
        } else {
            CheckUserExistance();
        }

    }

    private void CheckUserExistance() {
        final String current_user_id = mAuth.getCurrentUser().getUid();
        UsersRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (!dataSnapshot.hasChild(current_user_id)) {
                    SendUserToSetupActivity();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void SendUserToSetupActivity() {
        Intent setupIntent = new Intent(this, SetupActivity.class);
        setupIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(setupIntent);
        finish();
    }

    private void sendUserToLoginActivity() {
        Intent mLoginIntent = new Intent(MainActivity.this, LoginActivity.class);
        mLoginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mLoginIntent);
        finish();
    }

    private void sendUserToAboutActivity() {
        Intent aboutIntent = new Intent(MainActivity.this, AboutActivity.class);
        startActivity(aboutIntent);
    }
}
