package comexclusive.ozofttechnologies.exclusiveapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity {

    private TextView mUsersCounter, mNoAccountLink, mForgotPasswordLink;
    private EditText mLoginEmail, mLoginPassword;
    private FloatingActionButton mLoginButton;
    private Boolean mEmailAddressChecker;

    private FirebaseAuth mAuth;
    private ProgressDialog loadingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();

        //mUsersCounter = (TextView) findViewById(R.id.counter);
        mLoginEmail = (EditText) findViewById(R.id.login_input);
        mLoginPassword = (EditText) findViewById(R.id.login_password_input);
        mLoginButton = (FloatingActionButton) findViewById(R.id.nex_login_step_button);
        mNoAccountLink = (TextView) findViewById(R.id.not_member_text);
        mForgotPasswordLink = (TextView) findViewById(R.id.forgot_password_link);

        loadingBar = new ProgressDialog(this);

        mNoAccountLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendUserToRegisterActivity();
            }
        });

        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AllowingUserToLogin();
            }
        });

        mForgotPasswordLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendUserToResetPasswordActicity();
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser currentUser = mAuth.getCurrentUser();

        if (currentUser != null) {
            SendUserToMainActivity();
        }
    }

    private void verifyEmailAddress() {
        FirebaseUser mUser = mAuth.getCurrentUser();
        mEmailAddressChecker = mUser.isEmailVerified();

        if (mEmailAddressChecker) {
            SendUserToMainActivity();
        }
        else {
            Toast.makeText(LoginActivity.this, R.string.VerifyEmailMessage, Toast.LENGTH_LONG).show();
            mAuth.signOut();
        }
    }

    private void AllowingUserToLogin() {

        String email = mLoginEmail.getText().toString();
        String password = mLoginPassword.getText().toString();

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, R.string.WriteEmail, Toast.LENGTH_SHORT).show();
        }

        else if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, R.string.WritePassword, Toast.LENGTH_SHORT).show();
        }

        else {
            loadingBar.setTitle(R.string.LoginMessage);
            loadingBar
                    .setMessage(getApplication().getApplicationContext().getResources().getString(R.string.PleaseWait));
            loadingBar.show();
            loadingBar.setCanceledOnTouchOutside(true);
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if (task.isSuccessful()) {
                                verifyEmailAddress();
                                //Toast.makeText(LoginActivity.this, R.string.WelcomeText, Toast.LENGTH_SHORT).show();
                                loadingBar.dismiss();
                            }

                    else {
                                String message = task.getException().getMessage();
                                Toast.makeText(LoginActivity.this, R.string.ErrorMessage + message, Toast.LENGTH_SHORT)
                                        .show();
                                loadingBar.dismiss();

                            }
                        }
                    });
        }

    }

    private void SendUserToMainActivity() {
        Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
        finish();
    }

    private void sendUserToRegisterActivity() {
        Intent registerIntent = new Intent(this, RegisterActivity.class);
        startActivity(registerIntent);
    }

    private void sendUserToResetPasswordActicity() {
        Intent resetIntent = new Intent(this, ResetPasswordActivity.class);
        startActivity(resetIntent);
    }

}
