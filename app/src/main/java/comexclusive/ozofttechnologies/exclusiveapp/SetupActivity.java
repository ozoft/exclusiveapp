package comexclusive.ozofttechnologies.exclusiveapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class SetupActivity extends AppCompatActivity {

    private EditText mRegisterUsername;
    private FloatingActionButton mSetupButton;
    private ProgressDialog loadingBar;

    private FirebaseAuth mAuth;
    private DatabaseReference UsersRef, mUserNumber;
    String currentUserID, currentNumberOfUsers, newNumberOfUsers;
    Integer newNumberUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);

        mAuth = FirebaseAuth.getInstance();
        currentUserID = mAuth.getCurrentUser().getUid();
        UsersRef = FirebaseDatabase.getInstance().getReference().child("Users").child(currentUserID);
        mUserNumber = FirebaseDatabase.getInstance().getReference().child("numberOfUsers").child("number");
        mRegisterUsername = (EditText) findViewById(R.id.register_username_input);
        mSetupButton = (FloatingActionButton) findViewById(R.id.setup_button);
        loadingBar = new ProgressDialog(this);

        mSetupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SaveUserInformation();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        mUserNumber.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    currentNumberOfUsers = dataSnapshot.child("actualUsersNumber").getValue().toString();
                    newNumberUser = Integer.parseInt(currentNumberOfUsers) + 1;
                    newNumberOfUsers = String.valueOf(newNumberUser);
                } else {
                    newNumberOfUsers = "1";
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void SaveUserInformation() {
        String fullName = mRegisterUsername.getText().toString();

        if (TextUtils.isEmpty(fullName)) {
            Toast.makeText(SetupActivity.this, R.string.CreateUsername, Toast.LENGTH_SHORT).show();
        }

        else {

            loadingBar.setTitle(R.string.SavingData);
            loadingBar
                    .setMessage(getApplication().getApplicationContext().getResources().getString(R.string.PleaseWait));
            loadingBar.show();
            loadingBar.setCanceledOnTouchOutside(true);

            HashMap userMap = new HashMap();
            userMap.put("userName", fullName);
            userMap.put("memberNumber", newNumberOfUsers);

            UsersRef.updateChildren(userMap).addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if (task.isSuccessful()) {
                        updateNumberOfUsers();
                        SendUserToMainActivity();
                        Toast.makeText(SetupActivity.this, R.string.CongratsMessage, Toast.LENGTH_LONG).show();
                        loadingBar.dismiss();
                    }

                    else {
                        String message = task.getException().getMessage();
                        Toast.makeText(SetupActivity.this, R.string.ErrorMessage + message, Toast.LENGTH_LONG).show();
                        loadingBar.dismiss();
                    }
                }
            });
        }

    }

    private void updateNumberOfUsers() {
        HashMap numUserMap = new HashMap();
        numUserMap.put("actualUsersNumber", newNumberOfUsers);
        mUserNumber.updateChildren(numUserMap).addOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                if (task.isSuccessful()) {
                    Toast.makeText(SetupActivity.this, R.string.CongratsMessage, Toast.LENGTH_SHORT).show();
                } else {
                    String message = task.getException().getMessage();
                    Toast.makeText(SetupActivity.this, R.string.ErrorMessage + message, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void SendUserToMainActivity() {
        Intent mainIntent = new Intent(SetupActivity.this, MainActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
        finish();
    }

}
