package comexclusive.ozofttechnologies.exclusiveapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class RegisterActivity extends AppCompatActivity {

    private EditText mRegisterEmail, mRegisterPassword, mRegisterConfPassword;
    private FloatingActionButton mRegisterButton;
    private ProgressDialog loadingBar;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAuth = FirebaseAuth.getInstance();

        mRegisterEmail = (EditText) findViewById(R.id.register_email_input);
        mRegisterPassword = (EditText) findViewById(R.id.register_password_input);
        mRegisterConfPassword = (EditText) findViewById(R.id.register_confirm_password_input);
        mRegisterButton = (FloatingActionButton) findViewById(R.id.register_button);

        loadingBar = new ProgressDialog(this);

        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createNewAccount();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser currentUser = mAuth.getCurrentUser();

        if (currentUser != null) {
            SendUserToMainActivity();
        }
    }

    private void createNewAccount() {

        String email = mRegisterEmail.getText().toString();
        String password = mRegisterPassword.getText().toString();
        String confirmPassword = mRegisterConfPassword.getText().toString();

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, R.string.WriteEmail, Toast.LENGTH_SHORT).show();
        }

        else if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, R.string.WritePassword, Toast.LENGTH_SHORT).show();
        }

        else if (TextUtils.isEmpty(confirmPassword)) {
            Toast.makeText(this, R.string.WriteConfirmPassword, Toast.LENGTH_SHORT).show();
        }

        else if (!password.equals(confirmPassword)) {
            Toast.makeText(this, R.string.WriteMatchPassword, Toast.LENGTH_SHORT).show();
        }

        else {

            loadingBar.setTitle(R.string.RegisterMessage);
            loadingBar
                    .setMessage(getApplication().getApplicationContext().getResources().getString(R.string.PleaseWait));
            loadingBar.show();
            loadingBar.setCanceledOnTouchOutside(true);
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if (task.isSuccessful()) {
                                sendEmailVerification();
                                //Toast.makeText(RegisterActivity.this, R.string.AuthenticateCorrect, Toast.LENGTH_SHORT)
                                  //      .show();
                                loadingBar.dismiss();
                            }

                    else {
                                String message = task.getException().getMessage();
                                Toast.makeText(RegisterActivity.this, R.string.ErrorMessage + message,
                                        Toast.LENGTH_SHORT).show();
                                loadingBar.dismiss();

                            }

                        }
                    });
        }

    }

    private void sendEmailVerification() {
        FirebaseUser mUser = mAuth.getCurrentUser();

        if (mUser != null) {
            mUser.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        Toast.makeText(RegisterActivity.this, R.string.SendVerifyEmail, Toast.LENGTH_LONG).show();
                        SendUserToLoginActivity();
                        mAuth.signOut();
                    }
                    else {
                        String message = task.getException().getMessage();
                        Toast.makeText(RegisterActivity.this, R.string.ErrorMessage + message, Toast.LENGTH_SHORT)
                                .show();
                        mAuth.signOut();
                    }
                }
            });
        }
    }

    private void SendUserToMainActivity() {
        Intent mainIntent = new Intent(RegisterActivity.this, MainActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
        finish();
    }

    private void SendUserToLoginActivity() {
        Intent loginIntent = new Intent(RegisterActivity.this, LoginActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
        finish();
    }

}
